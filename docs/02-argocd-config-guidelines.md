# ArgoCD Guidelines

[[_TOC_]]


## Server-Side Apply

Server side apply avoids errors from [exceeding annotation size limits](https://github.com/argoproj/argo-cd/issues/820#issuecomment-1246960210). It is also best practice to use server-side apply to configure ownership of fields.

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
spec:
  syncPolicy:
    syncOptions:
    - ServerSideApply=true
```

## Pruning

Reference: https://argo-cd.readthedocs.io/en/stable/user-guide/sync-options/#no-prune-resources

To prevent resources from being deleted when removed from Git repository, we can use the following annotations on the resources.

```yaml
metadata:
  annotations:
    argocd.argoproj.io/sync-options: Prune=false
```

To prevent resources (eg. PVCs) from being deleted during app deletion, we can use the following annotations on the resources.

Note that even with `Prune=false` set, resources will still be deleted when the Application is deleted.

```yaml
metadata:
  annotations:
    argocd.argoproj.io/sync-options: Delete=false
```

We can combine sync options with comma-separated values.

```yaml
metadata:
  annotations:
    argocd.argoproj.io/sync-options: Prune=false,Delete=false
```

## Cascading delete of all resources in Application

Reference: https://argo-cd.readthedocs.io/en/stable/user-guide/app_deletion/#about-the-deletion-finalizer

The finalizer will ensure all resources in the application are deleted when application is deleted.

Do not set this finalizer if you do not want resources to be removed with the application.

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  finalizers:
  - resources-finalizer.argocd.argoproj.io
```

## Ensure Project is not deleted until not referenced by any Application

```yaml
apiVersion: argoproj.io/v1alpha1
kind: AppProject
metadata:
  finalizers:
  - resources-finalizer.argocd.argoproj.io
```

## Skip Application reconciliation (Pause)

Reference: https://argo-cd.readthedocs.io/en/stable/user-guide/skip_reconcile/#skip-application-reconcile

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  annotations:
    argocd.argoproj.io/skip-reconcile: "true"
```

## Allow Syncing CR when CRD is not created yet

Reference: https://argo-cd.readthedocs.io/en/stable/user-guide/sync-options/#skip-dry-run-for-new-custom-resources-types

This occurs when CRDs are not created in the same sync as the CR. This setting allows skipping CRs first, and then resyncing later when the CRDs have been created.

```yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: clusterissuer
  annotations:
    argocd.argoproj.io/hook: PostSync
    argocd.argoproj.io/sync-options: SkipDryRunOnMissingResource=true
```

## Allow managing Application outside argocd namespace

Prerequisites: argocd must be installed as **cluster-wide** instance, instead of **namespace-scoped** mode.

> **Note**: Only `Application` can be installed outside `argocd` namespace. `AppProject` can only reside in the `argocd` namespace.

1. Reconfigure argocd to manage additional namespaces via the startup parameters of `argocd-server` deployment and `argocd-application-controller` statefulset. This can be easily configured via the `argocd-cmd-params-cm` ConfigMap.

```yaml
data:
  # comma-separated values. Wildcard * is allowed.
  application.namespaces: app-team-one, app-team-two
```

```bash
# Restart workloads after updating ConfigMap
kubectl rollout restart -n argocd deployment argocd-server
kubectl rollout restart -n argocd statefulset argocd-application-controller
```

2. Create an AppProject that includes the Application in `.spec.sourceNamespaces`.

```yaml
kind: AppProject
apiVersion: argoproj.io/v1alpha1
metadata:
  name: project-one
  namespace: argocd
spec:
  sourceNamespaces:
  - namespace-one
```

3. Install Kubernetes RBAC (once globally) to allow argocd server to **modify** applications in other namespaces.

Download the files from https://github.com/argoproj/argo-cd/tree/master/examples/k8s-rbac/argocd-server-applications and apply them.


4. Optionally (but recommended), change the resource tracking method to `annotation+label` to prevent exceeding 63 character length limit on label values.

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: argocd-cm
data:
  # This tracking method is recommended when allowing apps from any namespaces
  application.resourceTrackingMethod: annotation+label
```

### Allow faster source code feedback loop with webhooks

We can configure Git webhooks to argocd to initiate syncs faster than the polling interval.

The payload URL configured in the Git provider should use the `/api/webhook` endpoint of your Argo CD instance (e.g. `https://argocd.example.com/api/webhook`).

### Allow managing helm via kustomize and HelmChartInflationGenerator

> This does not work in Artifactory as it does not support slashes in chart name.

Reference: https://codefresh.io/blog/using-argo-cds-new-config-management-plugins-to-build-kustomize-helm-and-more/

1. Create ConfigMap (`kustomize-with-helm.yaml`) to store custom plugin in `ConfigManagementPlugin` resource. Configuring plugins in `argocd-cm` is [deprecated from 2.8](https://argo-cd.readthedocs.io/en/latest/operator-manual/config-management-plugins/#migrating-from-argocd-cm-plugins).

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: kustomize-build-with-helm
  namespace: argocd
data:
  plugin.yaml: |
    apiVersion: argoproj.io/v1alpha1
    kind: ConfigManagementPlugin
    metadata:
      name: kustomize-build-with-helm
    spec:
      generate:
        command: [ "sh", "-c" ]
        args: [ "kustomize build --enable-helm --load-restrictor LoadRestrictionsNone" ]
```

2. Create patch file (`argocd-repo-server-patch.yaml`) for adding Config Management Plugin (CMP) to ArgoCD.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: argocd-repo-server
spec:
  template:
    spec:
      containers:
      - name: kustomize-build-with-helm
        command: [/var/run/argocd/argocd-cmp-server] # Entrypoint should be Argo CD lightweight CMP server i.e. argocd-cmp-server
        # image: busybox # This can be off-the-shelf or custom-built image
        image: alpine/k8s:1.26.8
        securityContext:
          runAsNonRoot: true
          runAsUser: 999
        volumeMounts:
        - mountPath: /var/run/argocd
          name: var-files
        - mountPath: /home/argocd/cmp-server/plugins
          name: plugins
        # Remove this volumeMount if you've chosen to bake the config file into the sidecar image.
        - mountPath: /home/argocd/cmp-server/config/plugin.yaml
          subPath: plugin.yaml
          name: kustomize-build-with-helm
        # Starting with v2.4, do NOT mount the same tmp volume as the repo-server container. The filesystem separation helps 
        # mitigate path traversal attacks.
        - mountPath: /tmp
          name: cmp-tmp
      volumes:
      - configMap:
          name: kustomize-build-with-helm
        name: kustomize-build-with-helm
      - emptyDir: {}
        name: cmp-tmp
```

3. Add configmap and patch file from above in `Kustomization.yaml` for ArgoCD deployment.

```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
namespace: argocd
resources:
- ...
- kustomize-with-helm.yaml # Adds our configmap
patches:
- path: argocd-repo-server-patch.yaml # Adds the sidecar to argocd-repo-server
```

4. Use custom plugin in `Application.yaml`

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: app-with-kustomize-enable-helm
spec:
  project: default
  destination:
    server: https://kubernetes.default.svc
  source:
    plugin:
      name: kustomize-build-with-helm  # Use the plugin
```

5. Kustomize can now use [HelmChartInflationGenerator](https://kubectl.docs.kubernetes.io/references/kustomize/builtins/#_helmchartinflationgenerator_) to manage helm charts.

### Application Sources

Try to keep to a single application source (using the object representation of `source` instead of the list representation of `sources`) as this will expose commit information on the UI which will be more helpful.

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
spec:
  # Do this
  source:
    repoURL: ...

  # Try not to do this (plural sources and list)
  sources:
  - repoURL: ...
```
