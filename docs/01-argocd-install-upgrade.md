# ArgoCD Installation and Upgrade Notes

[[_TOC_]]


## Overview

### Project Layout

```bash
argocd-repo/
├── apps/                                # 4) application deployments (can be in another repo)
│   └── <app>/
│       ├── base/
│       │   ├── kustomization.yaml
│       │   └── global-values.yaml       # optional: additional file for helm deployment
│       └── env/
│           ├── env-values.yaml          # optional: additional file for helm deployment
│           ├── helm-application.yaml    # optional: additional file for helm deployment
│           └── kustomization.yaml
├── deployment/
│   ├── apps/
│   │   ├── <env>/
│   │   │   └── env-values.yaml          # 3) helm values for deployment/apps to install apps/
│   │   └── global-values.yaml
│   └── argocd/
│       ├── <env>/
│       │   ├── application-apps.yaml    # 2) application to sync deployment/apps/ with helm
│       │   ├── application-argocd.yaml  # 1) entrypoint: application to sync deployment/argocd/
│       │   ├── env-values.yaml
│       │   └── kustomization.yaml
│       └── global-values.yaml
├── docs/                                # documentation
├── scripts/                             # helper scripts
└── README.md
```

### Workflow

ArgoCD installation is defined in the `deployment/argocd` folder. To kickstart the entire deployment, argocd is installed and the manifests in this folder has to be installed manually via kubectl. Subsequently, argocd takes over the management of this folder (and all dependent folders), and the entire project can be managed via code (ie. kubectl is no longer needed). 

`deployment/argocd/<env>/application-argocd.yaml` is the ArgoCD root application that serves as the entrypoint to the entire deployment. It installs ArgoCD via helm. It also syncs the kustomization folder at `deployment/argocd/<env>/` which includes itself, hence allowing ArgoCD to take control of itself.

`deployment/argocd/<env>/application-apps.yaml` uses a custom helm chart to install all desired apps. The helm chart abstracts away the complexities needed to configure ArgoCD Custom Resource manifests, by using helm values files in `deployment/apps/`. In this template, the helm values files are configured to install apps to the same repo at `/apps`. You can configure this to point to another Git repo, or multiple other Git repos.

The apps in `/apps` are structured in a 2-tier hierarchy for flexibility to configure global and environment-specific configurations.


## References

- [All ArgoCD Manifests](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/)


## New ArgoCD Setup

### Fork ArgoCD Template Repo

1. Fork the repo.

2. Modify the following values to match your environment. The sub-bullet points are the list of file/folders where the changes need to be made in. In this setup, we will configure ArgoCD to manage apps in the same repo. You may modify this yourself to manage apps in other repos instead.

    - Change the `k8s-dev` env **folder name** to match your environment (eg. if your env is `eck-dev`, change `deployment/argocd/k8s-dev` folder name to `deployment/argocd/eck-dev`).
      - `deployment/argocd/k8s-dev`
      - `deployment/apps/k8s-dev`
      - `apps/cert-manager/k8s-dev`
      - `apps/ingress-nginx/k8s-dev`
    - Change the `k8s-dev` env references in manifest files to match your environment. Can also use ctrl-shift-f to find `# change env folder`.
      - `apps/cert-manager/<env>/helm-application.yaml`
      - `deployment/apps/<env>/env-values.yaml`
      - `deployment/argocd/<env>/application-apps.yaml`
      - `deployment/argocd/<env>/application-argocd.yaml`
    - Change the `path/to/project` path references in manifest files to match your environment. Can also use ctrl-shift-f to find `# change project path`.
      - `apps/cert-manager/<env>/helm-application.yaml`
      - `deployment/apps/global-values.yaml`
      - `deployment/argocd/<env>/application-apps.yaml`
      - `deployment/argocd/<env>/application-argocd.yaml`
    - Change the versions of ArgoCD and apps (cert-manager, ingress-nginx) as necessary. Can also use ctrl-shift-f to find `# change version`.
      - `apps/cert-manager/<env>/helm-application.yaml`
      - `apps/ingress-nginx/<env>/helm-application.yaml`
      - `deployment/argocd/<env>/application-argocd.yaml`
    - Change the ArgoCD domain `argocd.dev.lan` to your desired domain. If necessary, also update the cert-manager issuer name (will also need to update the cert-manager app config). 
      - `deployment/argocd/<env>/env-values.yaml`

3. Commit the changes to your forked repo and push.

### Install ArgoCD to Kubernetes Cluster

> **Note:** Change variables in the scripts accordingly for your environment

1. Ensure you are in the correct `kubeconfig` context and bootstrap the initial installation.

```bash
# Check kubeconfig context
kubectl config current-context

# Add helm repository
helm repo add argo https://argoproj.github.io/argo-helm

# Install argocd 
VERSION=6.7.1
ENV=k8s-dev
helm template -n argocd argocd argo/argo-cd \
  --version ${VERSION} \
  -f deployment/argocd/global-values.yaml \
  -f deployment/argocd/${ENV}/env-values.yaml | kubectl apply -f -

# If you are following this template deployment without modification,
# you will get an error: no matches for kind "Certificate" in version "cert-manager.io/v1", ensure CRDs are installed first.
# This is normal as cert-manager is not installed yet. This will be resolved later.

# Install bootstrap Applications
kubectl apply -k deployment/argocd/${ENV}/
```

2. Download `argocd` cli from https://github.com/argoproj/argo-cd/releases/latest.

```bash
VERSION="v2.10.2"
curl -fsSLO https://github.com/argoproj/argo-cd/releases/download/${VERSION}/argocd-linux-amd64

mv argocd-linux-amd64 argocd
chmod u+x argocd
sudo mv argocd /usr/local/bin
```

3. Port forward

In lieu of ingress configurations, we can gain direct access first.

```bash
kubectl port-forward svc/argocd-server -n argocd 8443:443
```

Use `https://localhost:8443` to access ArgoCD temporarily. Accept the use of self-signed certs first.

4. Login via CLI and UI

```bash
# Obtain initial password
argocd admin initial-password -n argocd

# Login (replace server name and port accordingly)
ARGOCD_URL="localhost"
PORT=8443
argocd login ${ARGOCD_URL}:${PORT} --username admin

# Change password
argocd account update-password

# Visit https://${ARGOCD_URL}:${PORT} on the browser and login
```

5. Add git repo secret to read this repo. Obtain a deploy token with `read repository` permissions.

- Gitlab Project -> Settings -> Repository -> Deploy Tokens -> Add token
- Check `read_repository`. All other fields can be blank.
- Click `Create deploy token`.

```bash
PROJECT_PATH="path/to/project"
REPO="https://gitlab.com/${PROJECT_PATH}.git"  # .git suffix is important

# Add secret
./scripts/create-repo-secret.sh ${REPO} argocd <token-username> <token-password>

# Add ca cert (if necessary)
./scripts/create-repo-ca-certs.sh <repo-domain> <ca-url>
```

6. The port-forward would be killed as argocd would be restarted. Redo the port-forward command and check the ArgoCD UI that the apps are being updated. If you are using this template's apps, you need to make modifications to the cert manager app to get it to work. This requires having a PKI role and approle in Hashicorp Vault.

    - `/apps/cert-manager/<env>/cluster-issuer.yaml`:
      - update `spec.vault.path` for your vault PKI role
      - update `spec.vault.auth.approle.roleId` for your approle role ID
    - Follow instructions for initial setup in `apps/cert-manager/README.md` to install approle secret ID.

After the configurations are updated, we can wait for ArgoCD to eventually sync the configurations, or we can accelerate the process.

    - Go to Cert Manager Application in ArgoCD UI and delete `clusterissuer` clusterissuer resource.

The clusterissuer should be recreated successfuly. After that, ArgoCD server will restart again to update the cert.

At this point, we can use the original DNS domain name to access the cluster. If DNS has not been set up yet, you can put a temporary entry in `/etc/hosts` first.


## Operations

### Add New Kubernetes Cluster

In this approach, we install 1 ArgoCD server per Kubernetes cluster. This means that different ArgoCD domains are used to access different ArgoCD UI instances for each cluster.

1. Copy `deployment/argocd/<env>` to a new env folder.

```bash
# if you have an existing env = `k8s-dev` and you want to create a new env = `k8s-prod`
EXISTING_ENV=k8s-dev
NEW_ENV=k8s-prod
cp -r deployment/argocd/${EXISTING_ENV} deployment/argocd/${NEW_ENV}
```

2. Modify the relevant values in the newly copied folder.

    - `application-apps.yaml`:
      - change env folder in `spec.sources[0].helm.valueFiles[2]`. Eg. `$values/deployment/apps/k8s-dev/env-values.yaml` to `$values/deployment/apps/k8s-prod/env-values.yaml`
    - `application-argocd.yaml`:
      - change env folder in `spec.sources[0].helm.valueFiles[2]` (same as above)
      - change env folder in `spec.sources[2].path`. Eg. `deployment/argocd/k8s-dev` to `deployment/argocd/k8s-prod`.
    - `env-values.yaml`: change `global.domain` and `server.certificate.issuer.name` as necessary.

3. Copy `deployment/apps/<env>` to a new env folder.

```bash
# if you have an existing env = `k8s-dev` and you want to create a new env = `k8s-prod`
EXISTING_ENV=k8s-dev
NEW_ENV=k8s-prod
cp -r deployment/apps/${EXISTING_ENV} deployment/apps/${NEW_ENV}
```

4. Modify the relevant values in the newly copied folder.

    - `env-values.yaml`:
      - If this newly created prod environment mimics the existing dev environment, you can keep all the apps and just change all the `kustomize_path` values to point to the correct env folder.
      - Else, delete the entire app list and build a new one as desired.

5. Perform [Install ArgoCD to Kubernetes Cluster](#install-argocd-to-kubernetes-cluster) steps again using the new cluster env folder.

### Add New App

1. Add a new app element in `deployment/apps/${ENV}/env-values.yaml`. Follow the configuration guide in [argocd-apps-chart](https://gitlab.com/acidpizza-stuff/infra/argocd/argocd-apps-chart).

2. Add the app deployment kustomization config in the location pointed by the kustomize_path above. In this case where the app repo is hosted in the same argocd repo, we will add a new folder to `/apps`. The folder structure can be copied from the existing template apps `cert-manager` or `ingress-nginx`.

3. In general, the folder structure follows a 2-level kustomize config for flexibility to configure global and environment-specific configurations.

4. Helm deployments will require adding the following files.
    - `apps/<app>/<env>/helm-application.yaml`
    - `apps/<app>/<env>/env-values.yaml`
    - `apps/<app>/base/global-values.yaml`

### Fetch New Template Updates

If you had forked the template, you will have a notification on the main project page if there are upstream changes. While you can merge changes from the UI, it is better to do this manually on the command line to understand changes and have more control over merge conflicts.

```bash
# Add the template repository as an upstream to your forked repository.
git remote add upstream https://gitlab.com/acidpizza-stuff/infra/argocd/argocd-template

# Fetch changes
git fetch upstream

# Check changes from upstream
git difftool -t meld --dir-diff upstream

# Merge
git merge upstream/master

# Resolve merge conflicts if any
git mergetool -t meld
```

### Upgrade ArgoCD

ArgoCD can be upgraded by updating the `targetRevision` in the `application-argocd.yaml` manifest.

Read the [upgrade notes](https://argo-cd.readthedocs.io/en/stable/operator-manual/upgrading/overview/) before upgrading.

Check which helm chart version maps to which argocd app version.

```bash
helm search repo argo/argo-cd -l
```