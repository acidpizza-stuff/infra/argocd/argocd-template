# Helm Deployment Strategies

[[_TOC_]]


## References

- [github issue on helm chart and values from another Git repo](https://github.com/argoproj/argo-cd/issues/2789)
- [using ApplicationSets](https://www.softrams.com/post/external-value-files-in-argo-cd-with-applicationsets-softrams)


## Objectives

- Helm repository and values configuration should be done in the same Git repository.
- Helm configuration should be done in the app Git repo, not the ArgoCD Git repo. This allows apps to be updated without intervention from the ArgoCD admin, or updating the ArgoCD repo code.
- Do not repeat helm configuration for overlays.
- Use only one ArgoCD Application for an app.
- Project can control Application permissions.
- Kustomization can dynamically modify helm values (eg. using `nameReference` to modify ConfigMap names with suffix).


## CRDs

By default, helm Application will always install and upgrade CRDs. This can be disabled with `spec.source.helm.skipCrds: true`.


## Strategies

### Define Application with helm sources in ArgoCD Git repo

The ArgoCD admin defines the helm Application from the ArgoCD Git repo.

`application.yaml` in ArgoCD Git repo.

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: cert-manager-helm
  finalizers:
  # ensure all resources are deleted when Application is deleted
  - resources-finalizer.argocd.argoproj.io
spec:
  project: cert-manager

  sources:
  # helm repo
  - repoURL: https://charts.jetstack.io
    chart: cert-manager  
    targetRevision: 1.13.2
    helm:
      releaseName: cert-manager
      valueFiles:
      - $values/deployment/base/global-values.yaml
      - $values/deployment/overlay/cluster-values.yaml
  # helm values
  - repoURL: https://gitlab.com/path/to/app/repo.git
    targetRevision: HEAD
    ref: values
  # [optional] additional manifests to sync
  - repoURL: https://gitlab.com/path/to/app/repo.git
    targetRevision: HEAD
    path: ./deployment/overlay

  destination:
    server: https://kubernetes.default.svc
    namespace: cert-manager

  syncPolicy:
    automated:
      prune: true
      selfHeal: true
```

- ✅ Helm repository and values configuration can be done in the same Git repository
- ❌ Helm configuration must be done from the ArgoCD Git repository
- ❌ Helm configuration must be repeated for each overlay
- ✅ Use only one ArgoCD Application for an app
- ✅ Project can control Application permissions
- ❌ Kustomize cannot dynamically modify helm values since they are applied in different contexts

### Define Application in ArgoCD Git repo, but remote-patch helm sources from app Git repo

Each app Git repository defines a kustomization patch which the Application from ArgoCD Git repository will read remotely to add the helm source and `values.yaml` files.

`application.yaml` in ArgoCD Git repo.

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: cert-manager-helm
  finalizers:
  # ensure all resources are deleted when Application is deleted
  - resources-finalizer.argocd.argoproj.io
spec:
  project: cert-manager

  sources:
  - repoURL: https://gitlab.com/path/to/app/repo.git
    targetRevision: HEAD
    path: ./deployment/overlay

  destination:
    server: https://kubernetes.default.svc
    namespace: cert-manager

  syncPolicy:
    automated:
      prune: true
      selfHeal: true
```

`kustomization.yaml` in ArgoCD Git repo.

```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- application.yaml
- namespace.yaml
- project.yaml

patches:
# perform remote patch to add helm source
- target:
    group: argoproj.io
    version: v1alpha1
    kind: Application
    name: cert-manager
  path: https://gitlab.com/path/to/app/repo/deployment/overlay/application-helm-patch.yaml
```

`overlay/application-helm-patch.yaml` in app Git repo.

```yaml
# add helm repo
- op: add
  path: /spec/sources/-
  value:
    repoURL: https://charts.jetstack.io
    chart: cert-manager
    targetRevision: 1.13.2
    helm:
      releaseName: cert-manager
      valueFiles:
      - $values/deployment/base/global-values.yaml
      - $values/deployment/overlay/cluster-values.yaml

# add helm values
- op: add
  path: /spec/sources/-
  value:
    repoURL: https://gitlab.com/path/to/app/repo.git
    targetRevision: HEAD
    ref: values
```

- ✅ Helm repository and values configuration can be done in the same Git repository
- ✅ Helm configuration can be done from the app Git repository
- ❌ Helm configuration must be repeated for each overlay
- ✅ Use only one ArgoCD Application for an app
- ❌ Project cannot control Application permissions. Remote patch gives full access to ArgoCD application which can lead to privilege escalation (eg. can change project name).
- ❌ Kustomize cannot dynamically modify helm values since they are applied in different contexts
- ❌ Hard ArgoCD refresh needed to update helm application since no new commits are added to ArgoCD Git repo

### Define Kustomization Application in ArgoCD Git repo and Helm Application in app Git repo

Each app Git repository defines an ArgoCD Application with helm source. The app Git repository uses kustomization in overlay folders to update the chart version and add additional `values.yaml` files. 

`base/helm-application.yaml` in app Git repo.

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: cert-manager-helm
  finalizers:
  # ensure all resources are deleted when Application is deleted
  - resources-finalizer.argocd.argoproj.io
spec:
  project: cert-manager

  sources:
  - repoURL: https://charts.jetstack.io
    chart: cert-manager  
    targetRevision: dummy  # KUSTOMIZE
    helm:
      releaseName: cert-manager
      valueFiles:
      - $values/deployment/base/global-values.yaml
  - repoURL: https://gitlab.com/path/to/app/repo.git
    targetRevision: HEAD
    ref: values

  destination:
    server: https://kubernetes.default.svc
    namespace: cert-manager

  syncPolicy:
    automated:
      prune: true
      selfHeal: true
```

`overlay/kustomization.yaml` in app Git repo.

```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- ../base/
- cluster-issuer.yaml

namespace: cert-manager
    
patches:
- target:
    group: argoproj.io
    version: v1alpha1
    kind: Application
    name: cert-manager-helm
  patch: |
    # Update chart version
    - op: replace
      path: /spec/sources/0/targetRevision
      value: 1.13.2

    # Update chart values
    - op: add
      path: /spec/sources/0/helm/valueFiles/-
      value: $values/deployment/overlay/cluster-values.yaml
```

- ✅ Helm repository and values configuration can be done in the same Git repository
- ✅ Helm configuration can be done from the app Git repository
- ✅ Helm configuration can be reused in base for each overlay
- ❌ Additional ArgoCD helm Application has to be created in non-argocd namespace, resulting in double the number of Applications to manage.
- ✅ Project can control Application permissions.
- ✅ Kustomize can dynamically modify helm values via inline values in `helm-application.yaml`.

### Define Kustomization Application in ArgoCD Git repo and use HelmChartInflationGenerator in app Git repo

Kustomize allows the use of helm chart configuration in `kustomization.yaml` with `--enable-helm` and `--load-restrictor=LoadRestrictionsNone`.

`overlay/kustomization.yaml` in app Git repo.

```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- ../base/

# Cannot force namespace as helm chart installs to kube-system also
# namespace: cert-manager

# Only override metadata/namespace of all objects if not present
transformers:
- |-
  apiVersion: builtin
  kind: NamespaceTransformer
  metadata:
    name: namespacetransformer
    namespace: cert-manager
  setRoleBindingSubjects: none
  unsetOnly: true

helmCharts:
- name: cert-manager
  repo: https://charts.jetstack.io
  version: 1.13.2
  releaseName: cert-manager
  namespace: cert-manager
  includeCRDs: true
  valuesFile: ../base/global-values.yaml
  additionalValuesFiles:
  - cluster-values.yaml
```

- ✅ Helm repository and values configuration can be done in the same Git repository
- ✅ Helm configuration can be done from the app Git repository
- ❌ Helm configuration must be repeated for each overlay
- ✅ Use only one ArgoCD Application for an app
- ✅ Project can control Application permissions
- ❌ Kustomize cannot dynamically modify helm values, but can modify resources created from the helm chart
- ❌ HelmChartInflationGenerator requires creating a custom plugin
- ❌ HelmChartInflationGenerator does not perform the full features of helm
  - [Does not support slashes in chart name](https://github.com/kubernetes-sigs/kustomize/issues/3851)
  - Does not support providing ca certs and [needs workaround](https://github.com/argoproj/argo-cd/issues/3539#issuecomment-743333648)

### Define Helm Application in ArgoCD Git repo and use custom chart with dependencies in app Git repo

A custom helm chart can be used as a **proxy** to specify dependency subchart installations in `Chart.yaml` and additional manifests in `templates/`.

`application.yaml` in ArgoCD Git repo. The helm repo is a custom chart.

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: cert-manager
  finalizers:
  # ensure all resources are deleted when Application is deleted
  - resources-finalizer.argocd.argoproj.io
spec:
  project: cert-manager

  sources:
  # helm repo
  - repoURL: https://gitlab.com/path/to/app/repo.git
    chart: cert-manager  
    targetRevision: HEAD
    helm:
      releaseName: cert-manager
      valueFiles:
      - $values/deployment/base/global-values.yaml
      - $values/deployment/overlay/cluster-values.yaml
  # helm values
  - repoURL: https://gitlab.com/path/to/app/repo.git
    targetRevision: HEAD
    ref: values

  destination:
    server: https://kubernetes.default.svc
    namespace: cert-manager

  syncPolicy:
    automated:
      prune: true
      selfHeal: true
```

`Chart.yaml` in app Git repo.

```yaml
apiVersion: v2
name: dummy
version: 0.0.0
dependencies:
- name: cert-manager
  version: 1.13.2
  repository: https://charts.jetstack.io
```

`values.yaml` in app Git repo must be nested under the subchart name.

```yaml
cert-manager:
  installCRDs: true
  ...
```

- ✅ Helm repository and values configuration can be done in the same Git repository
- ❌ Helm configuration can be done from the app Git repository, but custom chart configuration must be done from the ArgoCD Git repository (eg. specify values.yaml paths)
- ❌ Helm configuration can be reused, but custom chart configuration must be repeated for each overlay
- ✅ Use only one ArgoCD Application for an app
- ✅ Project can control Application permissions
- ❌ Kustomize cannot dynamically modify helm values, but can modify resources created from the helm chart
- ❌ Issues with proxy chart
  - Additional complexity needed in managing a custom helm chart
  - Does not support authentication to private repository
  - App does not show actual helm chart version on UI
  - Chart values must be nested under the subchart name
