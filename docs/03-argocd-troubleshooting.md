# Troubleshooting

[[_TOC_]]


## Unable to sync git repository

Error: `Failed to sync: Unexpected client error: Unexpected requesting status code 301`.

This is because the repo secret's URL did not have a `.git` suffix. This is solved by updating the url to have `.git` suffix.


## Unable to sync kustomization folders recursively

Error: `The Kubernetes API could not find kustomize.config.k8s.io/Kustomization for requested resource`.

Using `recurse: true` in `application.yaml` will cause [all manifests to be treated as raw manifests](https://github.com/argoproj/argo-cd/issues/8695). The solution is to set `recurse: false` and point to a directory with kustomize, and then manually list all the desired files in the `kustomization.yaml`. This makes it less convenient than fluxCD.


## Cannot sync when `permitOnlyProjectScopedClusters: true`

The following error is obtained when project spec `permitOnlyProjectScopedClusters: true` is set: `application destination server 'https://kubernetes.default.svc' and namespace 'cert-manager' do not match any of the allowed destinations in project 'cert-manager'`. This is even when the correct cluster and namespace has already been defined in the project.

We need to define **project-scoped clusters** which is [not well documented](https://github.com/argoproj/argo-cd/pull/16210/files).
  
