#!/usr/bin/env bash

set -euo pipefail

if (( $# != 2 )); then
  echo "Create ca cert secret to trust repository."
  echo
  echo "Usage:"
  echo "${0} <repo-domain> <ca-url>"
  echo
  echo "- repo-domain: the repository domain name (eg. server.example.com)"
  echo "- ca-url: the url to download the root ca cert"
  echo
  exit 1
fi

repo_domain="${1}"
ca_url="${2}"

# get cert data and replace newlines with literal \n
ca_crt="$(curl -fsSL "${ca_url}" | sed 's/$/\\n/' | tr -d '\n')"

# get cert data and indent 4 spaces for manifest
# ca_crt="$(curl -fsSL "${ca_url}" | sed 's/^/    /g')"

# cat <<EOF | kubectl apply -f -
# apiVersion: v1
# kind: ConfigMap
# metadata:
#   name: argocd-tls-certs-cm
#   namespace: argocd
#   labels:
#     app.kubernetes.io/name: argocd-cm
#     app.kubernetes.io/part-of: argocd
# data:
#   ${repo_domain}: |
# ${ca_crt}
# EOF

# Create empty configmap
kubectl -n argocd create configmap argocd-tls-certs-cm -o yaml --dry-run=client | kubectl apply --server-side=true -f -

# Add labels
kubectl -n argocd patch configmap argocd-tls-certs-cm -p '{"metadata":{"labels":{"app.kubernetes.io/name": "argocd-cm", "app.kubernetes.io/part-of": "argocd"}}}'

# Add data
kubectl -n argocd patch configmap argocd-tls-certs-cm --type merge -p "{\"data\":{\"${repo_domain}\": \"${ca_crt}\"}}"