#!/usr/bin/env bash

set -euo pipefail

if (($# != 4 )); then
  echo "Create secret to read git repository."
  echo
  echo "Usage:"
  echo "${0} <repo-url> <namespace> <git-user> <git-token>"
  echo
  echo "- repo-url: Eg. https://gitlab.com/path/to/project.git"
  echo "- namespace: namespace to deploy secret"
  echo "- git-user: username to read git repo"
  echo "- git-token: deploy token to authenticate user to git repo"
  echo
  echo "Side effects:"
  echo "- secret name is 'gitrepo-<project name>'"
  echo
  exit 1
fi

repo_url="${1}"
namespace="${2}"
gitlab_user="${3}"
gitlab_token="${4}"

repo_name=$(basename ${repo_url})

cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: gitrepo-${repo_name}
  namespace: ${namespace}
  labels:
    argocd.argoproj.io/secret-type: repository
stringData:
  url: ${repo_url}
  username: ${gitlab_user}
  password: ${gitlab_token}
  insecure: "false"
  foceHttpBasicAuth: "true"
EOF
