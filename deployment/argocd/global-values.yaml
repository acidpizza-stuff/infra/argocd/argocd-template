# https://github.com/argoproj/argo-helm/blob/argo-cd-6.7.1/charts/argo-cd/values.yaml

crds:
  # -- Install and upgrade CRDs
  install: true
  # -- Keep CRDs on chart uninstall
  keep: true

global:
  logging:
    # -- Set the global logging format. Either: `text` or `json`
    format: json

## Argo Configs
configs:
  # General Argo CD configuration
  ## Ref: https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/argocd-cm.yaml
  cm:
    # This tracking method is recommended when allowing apps from any namespaces
    application.resourceTrackingMethod: annotation+label

  # Argo CD configuration parameters
  ## Ref: https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/argocd-cmd-params-cm.yaml
  params:
    # -- Enables [Applications in any namespace]
    ## List of additional namespaces where applications may be created in and reconciled from.
    ## The namespace where Argo CD is installed to will always be allowed.
    ## Set comma-separated list. (e.g. app-team-one, app-team-two)
    application.namespaces: "*"

  # Argo CD RBAC policy configuration
  ## Ref: https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/rbac.md
  rbac:
    # -- File containing user-defined policies and role definitions.
    # @default -- `''` (See [values.yaml])
    policy.csv: ''

  # ConfigMap for Config Management Plugins
  # Ref: https://argo-cd.readthedocs.io/en/stable/operator-manual/config-management-plugins/
  cmp:
    # -- Create the argocd-cmp-cm configmap
    create: false

  # -- Provide one or multiple [external cluster credentials]
  # @default -- `[]` (See [values.yaml])
  ## Ref:
  ## - https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/#clusters
  ## - https://argo-cd.readthedocs.io/en/stable/operator-manual/security/#external-cluster-credentials
  ## - https://argo-cd.readthedocs.io/en/stable/user-guide/projects/#project-scoped-repositories-and-clusters
  clusterCredentials: []

# -- Array of extra K8s manifests to deploy
## Note: Supports use of custom Helm templates
extraObjects:
- apiVersion: v1
  kind: Namespace
  metadata:
    name: argocd
    annotations:
      argocd.argoproj.io/sync-options: Prune=false,Delete=false

## Application controller
controller:
  # -- The number of application controller pods to run.
  # Additional replicas will cause sharding of managed clusters across number of replicas.
  ## With dynamic cluster distribution turned on, sharding of the clusters will gracefully
  ## rebalance if the number of replica's changes or one becomes unhealthy. (alpha)
  replicas: 1
  # -- Resource limits and requests for the application controller pods
  resources: {}
  ## Application controller metrics configuration
  metrics:
    # -- Deploy metrics service
    enabled: false

## Dex
dex:
  metrics:
    # -- Deploy metrics service
    enabled: false
  # -- Resource limits and requests for dex
  resources: {}

## Redis
redis:
  ## Prometheus redis-exporter sidecar
  exporter:
    # -- Enable Prometheus redis-exporter sidecar
    enabled: false
    # -- Resource limits and requests for redis-exporter sidecar
    resources: {}
  # -- Resource limits and requests for redis
  resources: {}
  metrics:
    # -- Deploy metrics service
    enabled: false
    serviceMonitor:
      # -- Enable a prometheus ServiceMonitor
      enabled: false

## Redis-HA subchart replaces custom redis deployment when `redis-ha.enabled=true`
# Ref: https://github.com/DandyDeveloper/charts/blob/master/charts/redis-ha/values.yaml
redis-ha:
  # -- Enables the Redis HA subchart and disables the custom Redis single node deployment
  enabled: false
  ## Prometheus redis-exporter sidecar
  exporter:
    # -- Enable Prometheus redis-exporter sidecar
    enabled: false

## Server
server:
  # -- The number of server pods to run
  replicas: 1
  ## Argo CD extensions
  ## This function in tech preview stage, do expect instability or breaking changes in newer versions.
  ## Ref: https://github.com/argoproj-labs/argocd-extension-installer
  ## When you enable extensions, you need to configure RBAC of logged in Argo CD user.
  ## Ref: https://argo-cd.readthedocs.io/en/stable/operator-manual/rbac/#the-extensions-resource
  extensions:
    # -- Enable support for Argo CD extensions
    enabled: false
    # -- Resource limits and requests for the argocd-extensions container
    resources: {}
  # -- Resource limits and requests for the Argo CD server
  resources: {}
  # TLS certificate configuration via cert-manager
  ## Ref: https://argo-cd.readthedocs.io/en/stable/operator-manual/tls/#tls-certificates-used-by-argocd-server
  certificate:
    # -- Deploy a Certificate resource (requires cert-manager)
    enabled: true
    issuer:
      # -- Certificate issuer group. Set if using an external issuer. Eg. `cert-manager.io`
      group: "cert-manager.io"
      # -- Certificate issuer kind. Either `Issuer` or `ClusterIssuer`
      kind: "ClusterIssuer"
      # -- Certificate issuer name. Eg. `letsencrypt`
      name: ""
    privateKey:
      # -- Algorithm used to generate certificate private key. One of: `RSA`, `Ed25519` or `ECDSA`
      algorithm: RSA
      # -- Key bit size of the private key. If algorithm is set to `Ed25519`, size is ignored.
      size: 2048
  ## Server metrics service configuration
  metrics:
    # -- Deploy metrics service
    enabled: false
    serviceMonitor:
      # -- Enable a prometheus ServiceMonitor
      enabled: false
  # Argo CD server ingress configuration
  ingress:
    # -- Enable an ingress resource for the Argo CD server
    enabled: true
    # -- Additional ingress annotations
    ## Ref: https://argo-cd.readthedocs.io/en/stable/operator-manual/ingress/#option-1-ssl-passthrough
    annotations:
      nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
      nginx.ingress.kubernetes.io/ssl-passthrough: "true"
    # -- Defines which ingress controller will implement the resource
    ingressClassName: "nginx"
    # -- Enable TLS configuration for the hostname defined at `server.ingress.hostname`
    ## TLS certificate will be retrieved from a TLS secret `argocd-server-tls`
    ## You can create this secret via `certificate` or `certificateSecret` option
    tls: true

## Repo Server
repoServer:
  # -- The number of repo server pods to run
  replicas: 1
  # -- Resource limits and requests for the repo server pods
  resources: {}
  ## Repo server metrics service configuration
  metrics:
    # -- Deploy metrics service
    enabled: false
    serviceMonitor:
      # -- Enable a prometheus ServiceMonitor
      enabled: false

## ApplicationSet controller
applicationSet:
  # -- The number of ApplicationSet controller pods to run
  replicas: 1
  ## Metrics service configuration
  metrics:
    # -- Deploy metrics service
    enabled: false
    serviceMonitor:
      # -- Enable a prometheus ServiceMonitor
      enabled: false
  # -- Resource limits and requests for the ApplicationSet controller pods.
  resources: {}

## Notifications controller
notifications:
  metrics:
    # -- Enables prometheus metrics server
    enabled: false
    serviceMonitor:
      # -- Enable a prometheus ServiceMonitor
      enabled: false
  # -- Resource limits and requests for the notifications controller
  resources: {}