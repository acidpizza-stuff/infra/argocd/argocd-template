# Cert Manager

[[_TOC_]]


## Commands

```bash
# Check status
kubectl -n cert-manager get clusterissuer

# Troubleshoot if have issues
kubectl -n cert-manager describe clusterissuer

# Delete clusterissuer before retrying deployment (setup-cert-manager.sh)
kubectl -n cert-manager delete clusterissuer xxx
```


## Setup with ArgoCD

```bash
# Obtain secret id from vault approle
read -s vault_approle_secretid

# Create secret for vault-cert-manager integration
kubectl -n cert-manager create secret generic "cert-manager-vault-approle" --from-literal=secretId="${vault_approle_secretid}" --dry-run=client -o yaml | kubectl apply --server-side=true -f -
```


## Update Cert Manager

Upgrade cert-manager [one minor version at a time](https://cert-manager.io/docs/installation/upgrade/), choosing the latest patch version for the minor version.

```bash
helm repo add "jetstack" "https://charts.jetstack.io"
helm search repo jetstack/cert-manager -l
```
